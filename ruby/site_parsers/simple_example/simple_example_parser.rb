#encoding:utf-8
require_relative '../framework/parser'

module SimpleExample
  class SimpleExampleParser < Framework::Parser
    def initialize_settings
      with_url('some_example_uri')
        .with_category_links('#menu-new #menu-item-1297 .sub-menu .menu-item > a')
        .with_category_links('#menu-new #menu-item-1298 .sub-menu .menu-item > a')
        .with_category_links('#menu-new #menu-item-6150 .sub-menu .menu-item > a')
        .with_category_links('http://artelio.ru/?product_cat=leg', true)
        .with_category_links('http://artelio.ru/?product_cat=swimsuit', true)
        .with_pagination('.next')
        .with_product_config(->(pc){
          pc.with_product_selector('#products-grid > li > a[href]')
            .with_category(->(product_page){
              prepare_string product_page.search('[itemprop=breadcrumb] > a:last-of-type').first.text
            })
            .with_name(->(product_page){
              prepare_string product_page.search('h1').first.text
            })
            .with_articul(->(product_page){
              product_page.search('[data-product_id]').first.attr('data-product_id').to_s
            })
            .with_price(->(product_page){
              # футболки - 800 свитшоты - 1600 лонгсливы - 900 леггинсы - 1100 купальники - 1300
              category = prepare_string product_page.search('[itemprop=breadcrumb] > a:last-of-type').first.text
              category = category.mb_chars.downcase.to_s
              return 800 if category.include? 'футболк'
              return 1600 if category.include? 'свитшот'
              return 900 if category.include? 'лонгслив'
              return 1100 if category.include? 'леггинс'
              return 1300 if category.include? 'купальник'
            })
            .with_sizes(->(product_page){
              div = product_page.search('.variations_lines').find{|div|div.search('.label label').first.text == 'Размер'}

              return [] if div.nil?

              div.search('option').drop(1).map{|opt|prepare_string opt.text}
            })
            .with_description(->(product_page){
              product_page.search('.product_description p').map{|p| prepare_string p.text}.join ' '
            })
            .with_images(->(product_page){
              product_page.search('.fresco').map{|a|a.attr('href').to_s}
            })
            .with_image_config(->(ic){
              ic.with_default_url('some_example_uri')
            })
            .with_category_type('850')
        })
    end
  end
end
#encoding:utf-8

class ProductUpdateWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def initialize
    super
    _create_functions
  end

  def perform(parser_data_id)
    @parser_data = ParserData.find parser_data_id
    return if @parser_data.parser_id == -1
    @parser_service = Parser::ParserService.new(@parser_data)
    @functions[@parser_data.status].call
  end

  private
  def _create_functions
    @functions = {
        'success' => ->(){
          # Здесь мы проверяем количество времени, прошедшее
          # между последним успешно завершенным циклом парсинга
          # и последней загрузкой данных
          # Запускаем start_download и меняем статус на parsing
          @parser_data.reload
          data = @parser_service.get_folder_data
          return if @parser_data.interval_hours.nil? or @parser_data.interval_hours < 1
          return if ((Time.now - Time.parse(data['upload_data'])) / 1.hour) < @parser_data.interval_hours
          begin
            @parser_service.start_download
          rescue => e
            @parser_data.update_attributes({:status => 'error_at_parse', :error_message => e.message})
          end
        },
        'parsing' => ->(){
          # Здесь мы проверяем ход парсинга
          # Если статус 1, то просто идем дальше
          # Если статус 0 и нет сообщения об ошибке
          # (может, просто флаг добавить в purchase еще один),
          # то меняем статус на uploading
          # если есть сообщение об ошибке, то меняем статус на error_at_parse
          data = @parser_service.get_folder_data
          return if data['status'].nil?
          return if data['status'] > 0
          is_ok = data['message'].include? 'успешно'
          if is_ok
            @parser_data.update_attribute(:status, 'start_upload')
          else
            @parser_data.update_attributes({:status => 'error_at_parse', :error_message => data['message']})
          end
        },
        'error_at_parse' => ->(){
          # Здесь мы ставим флаг pause в true
          # Если этот флаг уже стоит, то сообщение об ошибке не отправляется
          # В противном случае, конечно же, отправляется на почту
          # Этот флаг мы можем изменить только вручную при нажатии кнопки на сайте
          # И тогда parser_data переходит в статус uploading
          @parser_data.pause_update
          ProductUpdateMailer.delay.error_at_parse(@parser_data, @parser_data.error_message)
        },
        'start_upload' => ->(){
          # Здесь мы просто запускаем difference_worker или как его там
          # По окончании меняем статус на success
          @parser_data.update_attribute(:status, 'uploading')
          @parser_service.get_difference
        },
        'uploading' => ->(){
          # оставляем пустой, потому что при этом статусе происходит загрузка товаров на сайт
        },
        'error_at_upload' => ->(){
          # Этот статус получается из uploading
          # Здесь мы точно так же ставим флаг pause в true и отправляем сообщение,
          # если этого флага не стояло
          @parser_data.pause_update
          ProductUpdateMailer.delay.error_at_upload(@parser_data, @parser_data.error_message)
        },
    }
  end
end
#encoding:utf-8
require_relative 'product_validator'
module Framework
  class SizeableProductValidator < Framework::ProductValidator
    def initialize
      super
      add_function(->(product) { product.sizes.nil? or product.sizes.length==0 }, 'ERROR: Product sizes is nil')
      add_function(->(product) { product.sizes.any? { |s| s.nil? or s == '' } }, 'ERROR: Some of product sizes is not defined')
    end
  end
end
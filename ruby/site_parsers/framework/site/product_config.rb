#encoding:utf-8
require_relative '../parsed_product'
require_relative '../sizeable_parsed_product'
require_relative '../image_config'
require_relative '../product_validator'

module Framework
  module Site
    class ProductConfig
      def initialize
        @collections=Hash.new
        @category_types = []
        @category_types_by_name = []
        @default_category_type = ''
        @product_selectors = []
        @validator = Framework::ProductValidator.new
        @max_collection_length = 200
        # Функции-заглушки
        @description_func = ->(product) { '' }
        @name_func = ->(product) { '' }
        @get_images = ->(product) { [] }
        @price_func = ->(product) { 0 }
        @articul_func = ->(product) { '' }
        @category_func = ->(product) { '' }
        @get_sizes = nil
        @get_sizes_table = ->(elem) { [] }
        @with_logging = false
        @with_separate_page = true
        @log_func = ->(msg) { puts msg }
        @error_log_func = ->(msg) { puts msg }
      end

      def run(agent, category_page)
        @agent = agent
        initialize_image_config
        product_elements = get_product_elements(category_page)
        product_elements.map { |product_elem|
          if @with_separate_page
            link = product_elem.attr('href').to_s
            @log_func.call link if @with_logging
            product_page = get_page(link)
            create_new_product product_page, link
          else
            create_new_product product_elem
          end
        }
      end

      def get_product_elements(category_page)
        @product_selectors.map { |getter| getter.is_a?(String) ? category_page.search(getter) : getter.call(category_page) }.flatten
      end

      def initialize_image_config
        @image_config.with_agent(@agent).with_picture_path(@picture_path).with_logging(->(msg) { @log_func.call msg }, ->(msg) { @error_log_func.call msg })
      end

      def get_page(link)
        @agent.get(link)
      end

      def without_separate_page
        @with_separate_page = false
        self
      end

      def with_logging(log_func=nil, error_log_func=nil)
        @with_logging = true
        @log_func = log_func unless log_func.nil?
        @error_log_func = error_log_func unless error_log_func.nil?
        self
      end

      def with_unique(unique_func)
        @unique_func = unique_func
        self
      end

      def parse_product(product_elem, link = nil)
        result = nil
        articul = get_articul(product_elem)
        if @get_sizes.nil?
          result = Framework::ParsedProduct.new(@category_func.call(product_elem), articul, save_images(product_elem, articul),
                                                @name_func.call(product_elem), @description_func.call(product_elem), @price_func.call(product_elem), link)
        else
          result = Framework::SizeableParsedProduct.new(@category_func.call(product_elem), articul, save_images(product_elem, articul),
                                                        @name_func.call(product_elem), @description_func.call(product_elem), @price_func.call(product_elem),
                                                        @get_sizes.call(product_elem), link)
          result.sizes_table = @get_sizes_table.call(product_elem)
        end
        result.retail_coefficient = @retail_coefficient unless @retail_coefficient.nil?
        result.markup_coefficient = @markup_coefficient unless @markup_coefficient.nil?
        result.producer = @producer_func.call(product_elem) unless @producer_func.nil?
        result.composition = @composition_func.call(product_elem) unless @composition_func.nil?
        result.sizes_link = @sizes_link_func.call(product_elem) unless @sizes_link_func.nil?
        result
      end

      def create_new_product(product_elem, link = nil)
        products = parse_product(product_elem, link)
        @purchase.check_stop
        if products.nil?
          @error_log_func.call "Product is nil for link #{link} and element #{product_elem}"
          return
        end
        products = [products] unless products.kind_of? Array
        products.each { |product|
          log_product(product) if @with_logging
          if is_product_unique(product)
            add_product(product)
            add_product_new(product)
          end
        }
      end

      def validate_product(product)
        @validator.validate product
      end

      def is_product_unique(product)
        return true if @unique_func.nil?
        @unique_func.call(product)
      end

      def get_json(product, color = '', color_image=nil)
        color = '' if color.nil?
        json = {'sizes' => product.get_json_sizes, 'pics' => product.images.map { |i| i.to_json }}
        json['color_image'] = color_image.to_json unless color_image.nil?
        {color => json}
      end

      def save_images(product_elem, articul)
        saved_images = []
        @get_images.call(product_elem).each_with_index { |url, i|
          result = @image_config.run(url, articul, i)
          saved_images << result unless result.nil?
        }
        saved_images
      end

      ### METHODS FOR WORKING WITH DATABASE

      def get_collection_name_from_category_map(category, name)
        result = category
        cat_map = CategoryMap.where(cat_type: get_category_type(result, name)).first

        unless cat_map.nil?
          cat_type = cat_map.name.split('/').map { |a| prepare_string a }.take(3)
          cat_type[1] = 'Детская' if cat_type[0] == 'Обувь' and (cat_type[1].include?('мальчиков') or cat_type[1].include?('девочек'))
        end

        if get_category_type_by_category(result) != cat_map.cat_type
          result = cat_map.name.split('/').map { |a| prepare_string a }
          result = "#{result[0]}, #{result[-1]}"
        end
        result
      end

      def add_collection(category, name=nil)
        type = get_category_type_by_category(category)
        if type == @default_category_type and not name.blank?
          type = get_category_type(category, name)
        end

        name1=category.strip

        (1..100).each do |i|

          category="#{name1} - #{i}" if i>1

          if @collections.include?(category)
            @collections[category].reload
            puts @collections[category].products.length
            return @collections[category] if @collections[category].products.length<@max_collection_length
            next
          end

          col=Collection.new
          col.name=category
          col.coltype=type
          col.purchase=@purchase
          col.save

          @collections[category]=col
          return @collections[category]
        end
      end

      def get_category_type(category, name)
        result = get_category_type_by_category(category)

        result = get_category_type_by_name("#{category} #{name}") if result == @default_category_type

        result
      end

      def get_category_type_by_category(category)
        category_lower = category.mb_chars.downcase.to_s
        elem = @category_types.find { |k| k[0].all? { |w| category_lower.include? w } }
        return elem[1] unless elem.nil?
        elem.nil? ? @default_category_type : elem[1]
      end

      def get_category_type_by_name(name)
        name_lower = name.mb_chars.downcase.to_s
        elem = @category_types_by_name.find { |k| k[0].all? { |w| name_lower.include? w } }
        elem.nil? ? @default_category_type : elem[1]
      end


      def with_category_type(type, *words_to_include)
        if words_to_include.length == 0
          @default_category_type = type
          return self
        end
        @category_types.push [words_to_include.map(&:clone), type]
        self
      end

      def with_category_type_by_name(type, *words_to_include)
        if words_to_include.length == 0
          return self
        end
        @category_types_by_name.push [words_to_include.map(&:clone), type]
        self
      end

      def get_new_category_type(type)
        cat_map = CategoryMap.where(cat_type: type).first

        return [] if cat_map.nil?

        cat_type = cat_map.name.split('/').map { |a| prepare_string a }
        cat_type[1] = 'Детская' if cat_type[0] == 'Обувь' and (cat_type[1].include?('мальчиков') or cat_type[1].include?('девочек'))

        if cat_type[0] == 'Для дома'
          cat_type[0] = 'Товары для дома'

          cat_type[2] = cat_type[-1] if cat_type.length > 2
        end

        if cat_type[0] == 'Детям' and cat_type[1] == 'Одежда'
          cat_type.delete_at(1)
        end

        if cat_type.length > 2 and cat_type[1] == 'Одежда' and cat_type[2].include? 'Верхняя'
          cat_type.delete_at(1)
        end

        cat_type.take(3)
      end

      def add_product_new(product, json=nil, cat_type=nil)
        product.retail_coefficient = @retail_coefficient unless @retail_coefficient.nil?
        json = get_json(product) if json.nil?
        category = product.category
        if cat_type.nil?
          cat_type = get_new_category_type(get_category_type(category, product.name))

          cat_map = CategoryMap.where(cat_type: get_category_type(category, product.name)).first

          if get_category_type_by_category(category) != cat_map.cat_type
            category = cat_map.name.split('/').map { |a| prepare_string a }
            category = "#{category[0]}, #{category[-1]}"
          end
        end

        prod=ProductNew.new
        prod.url=product.link
        prod.cat=category
        prod.cat_type=cat_type.to_json
        prod.sku=product.articul
        prod.name=product.name
        prod.desc=product.description
        prod.colors=json.to_json
        prod.purchase=@purchase
        prod.sizes_table=product.sizes_table.to_json if product.is_a? Framework::SizeableParsedProduct
        prod.additional = product.additional.to_json
        prod.producer = product.producer
        prod.composition = product.composition
        prod.sizes_link = product.sizes_link
        prod.save
        @purchase.check_stop
        prod
      end

      def add_product(product)
        prod=Product.new
        prod.art=product.articul
        prod.name=product.name
        prod.price=product.price
        prod.desc=product.description

        if not product.description.mb_chars.downcase.to_s.include? 'состав' and not product.composition.blank?
          prod.desc = "#{prod.desc} Состав: #{product.composition}"
        end

        if not product.description.mb_chars.downcase.to_s.include? 'производитель' and not product.producer.blank?
          prod.desc = "#{prod.desc} Производитель: #{product.producer}"
        end

        prod.sizes=product.sizes.map{|s|s.gsub(',','.')}.join(',') if product.is_a? Framework::SizeableParsedProduct
        prod.error = validate_product product
        images = product.images.map { |img| img.path }
        if images
          if images.length>0 and images[0].to_s.starts_with? '/'
            prod.pics=images.join('~~~')
          else
            prod.pics=images.map { |p| "#{@picture_path}#{p}.jpg" }.join('~~~')
          end
        else
          prod.pics=''
        end
        prod.collection=add_collection(product.category, product.name)
        prod.save
        prod.collection.save
        @purchase.check_stop
        prod
      end

      def log_product(product)
        @log_func.call 'LOG PRODUCT'
        @log_func.call "Category:#{product.category}"
        @log_func.call "Name:#{product.name}"
        @log_func.call "Articul:#{product.articul}"
        @log_func.call "Price:#{product.price}"
        @log_func.call "Description:#{product.description}"
        if product.images.nil? or not product.images.length
          @log_func.call 'Images length 0'
        else
          @log_func.call "Images length:#{product.images.length}"
          product.images.each { |i| @log_func.call i }
        end
        if product.is_a? Framework::SizeableParsedProduct
          if product.sizes.nil? or not product.sizes.length
            @log_func.call 'Sizes length 0'
          else
            @log_func.call "Sizes length:#{product.sizes.length}"
            product.sizes.each { |i| @log_func.call i }
          end
        end
        @log_func.call 'END LOG PRODUCT'
      end

      ### METHODS FOR CONFIGURATION
      def with_sizes(get_sizes)
        @get_sizes = get_sizes || -> { [] }
        self
      end

      def with_category(category_func)
        @category_func = category_func || ->(product_elem) { '' }
        self
      end

      def get_articul(product_elem)
        articul_text = @articul_func.call(product_elem)

        if @articul_regex.nil?
          return articul_text
        end

        return '' if articul_text.match(@articul_regex).nil?

        articul_text.match(@articul_regex)['articul']
      end

      def with_product_selector(selector)
        @product_selectors << selector
        self
      end

      def with_price(price_func)
        @price_func = price_func || -> { '' }
        self
      end

      def with_name(name_func)
        @name_func = name_func || -> { '' }
        self
      end

      def with_description(descr_func)
        @description_func = descr_func || -> { '' }
        self
      end

      def with_articul(articul_func, articul_regex=nil)
        @articul_func = articul_func || -> { '' }
        @articul_regex = articul_regex
        self
      end

      def with_images(get_images)
        @get_images = get_images || -> { [] }
        self
      end

      def with_purchase(purchase)
        @purchase = purchase
        self
      end

      def with_picture_path(picture_path)
        @picture_path = picture_path
        self
      end

      def with_image_config(image_config_func, image_config_class = Framework::ImageConfig)
        @image_config = image_config_func.call(image_config_class.new)
        self
      end

      def with_validator(validator_class=Framework::ProductValidator, validator_func=nil)
        if validator_func.nil?
          @validator = validator_class.new
        else
          @validator = validator_func.call(validator_class.new)
        end
        self
      end

      def with_max_collection_length(length)
        @max_collection_length = length
        self
      end

      def with_sizes_table(sizes_table_func)
        @get_sizes_table = sizes_table_func
        self
      end

      def with_retail_coefficient(retail_coefficient)
        @retail_coefficient = retail_coefficient
        self
      end

      def with_markup_coefficient(markup_coefficient)
        @markup_coefficient = markup_coefficient
        self
      end

      def with_producer(producer_func)
        @producer_func = producer_func
        self
      end

      def with_composition(composition_func)
        @composition_func = composition_func
        self
      end

      def with_sizes_link(sizes_link_func)
        @sizes_link_func = sizes_link_func
        self
      end
    end
  end
end

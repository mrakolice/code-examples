#encoding:utf-8
require_relative 'base_site_job'
require 'csv'

module Framework
  class OrderSender < Framework::BaseSiteJob
    attr_accessor :order_csv
    attr_accessor :for_new_site
    attr_accessor :agent_header
    attr_accessor :need_agent_header

    def initialize
      super
      @dlparams = {'for_new_site' => :checkbox, 'order_csv' => :file}.merge!(@dlparams)
      @need_agent_header = false
      @csv_data = {}
      @error_csv_data = []
    end

    def add_error_csv_data(reason, articul, color=nil, size=nil, quantity=nil)
      @error_csv_data << [articul, color, size, quantity, reason]
      error_func "ERROR: #{reason}. Articul: #{articul}, color: #{color}, size: #{size}"
    end

    def debug_func(msg)
      puts msg
      @log.debug msg
    end

    def error_func(msg)
      puts msg
      @log.error msg
    end

    def process
      @agent.user_agent = @agent_header if @need_agent_header
      parse_data
      @csv_data.each do |art, sizes|
        @purchase.check_stop
        order(art, sizes)
      end
      puts @agent.cookies
      @purchase.cookies = @agent.cookies.map{|c|"#{c.name}=#{c.value}"}.join '; '
      @purchase.save!
      create_wrong_csv_output
    end

    def get_headers
      ['Articul', 'Color', 'Size', 'Quantity', 'Reason']
    end

    def create_wrong_csv_output
      return if @error_csv_data.length == 0

      @purchase.message = "Не найдены некоторые товары. Проверьте последний файл wrong_data в #{@file_path}"
      @purchase.save

      file_name = @file_path + 'wrong_data_' + Time.now.strftime('%Y-%m-%d_%H-%M') + '.csv'

      CSV.open(file_name, 'wb',
               :write_headers => true,
               :col_sep => ';',
               :headers => get_headers#< column header
      ) do |headers|
        @error_csv_data.each do |f|
          headers << [f[0], f[1], f[2], f[3], f[4], f[5]]
        end
      end
    end

    def order(articul, quantity)
    end

    def parse_data
      CSV.foreach(@order_csv, :col_sep => ';', :headers => true, :encoding => 'Windows-1251:utf-8') do |fields|
        if @for_new_site
          add_csv_data_for_new_site(fields)
          next
        end
        # Сюда идет если выгрузка
        next if fields['Статус']!='неподтвержден'
        add_csv_data(fields)
      end
    end

    def add_csv_data(fields)
      art=fields['Товар'].to_s
      @csv_data[art]+=1 if @csv_data.include? art
      @csv_data[art]=1 unless @csv_data.include? art
    end

    def add_csv_data_for_new_site(fields)
      art=fields['Артикул'].to_s
      @csv_data[art]+=1 if @csv_data.include? art
      @csv_data[art]=1 unless @csv_data.include? art
    end

    def success(job)
      super
      return if @error_csv_data.length == 0

      @purchase.message = "Не найдены некоторые товары. Проверьте последний файл wrong_data в #{@file_path}"
      @purchase.save
    end
  end
end
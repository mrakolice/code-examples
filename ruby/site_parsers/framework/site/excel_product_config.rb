#encoding:utf-8
require_relative 'product_config'

module Framework
  module Site
    class ExcelProductConfig < Framework::Site::ProductConfig
      def add_product(product)
        product.images = save_images(product)
        super product if product.images.length > 0
      end

      def save_images(product)
        saved_images = []
        product.images.each_with_index { |url, i|
          result = @image_config.run(url, product.articul, i)
          saved_images << result unless result.nil?
        }
        saved_images
      end

      def with_agent(agent)
        @agent = agent
        initialize_image_config
      end
    end
  end
end
#encoding:utf-8
require_relative 'site/authorization_data'
require_relative 'extensions'
require 'mechanize'
module Framework
  class BaseSiteJob
    attr_accessor :purchase_id
    attr_reader :dlparams

    def initialize
      @dlparams = {} if @dlparams.nil?
      @auth_data = nil
    end

    def purchase_id=(purchase_id)
      @file_path=Spup2::Application.config.data_path+self.class.name.gsub('::', '_')+ "_#{purchase_id}/"
      FileUtils.mkpath(@file_path) unless File.exists? @file_path
      @purchase_id=purchase_id
    end

    def with_auth_data(auth_data_func)
      @auth_data = auth_data_func.call(Framework::Site::AuthorizationData.new)
      self
    end

    def run
      initialize_settings

      authorize

      clean

      process
    end

    def authorize
      @auth_data.run(@agent) unless @auth_data.nil?
    end

    def clean

    end

    def initialize_settings

    end

    def process

    end

    def before
      @purchase = Purchase.find(@purchase_id)

      unless @purchase.nil?
        @purchase.status=0 if @purchase.status==nil
        raise "Закупка не готова к обработке" if @purchase.status!=10

        @purchase.status=1
        @purchase.message="Идет загрузка из источника"
        @purchase.save
      end
      @log=Logger.new("log/dl-#{self.class.name.gsub('::', '_')}.txt")
      @log.level = Logger::DEBUG

      create_agent
    end

    def create_agent
      @agent = Mechanize.new
      @agent.agent.http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      @agent.user_agent='Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'
      @agent
    end

    def after(job)
      @purchase.reload
      @purchase.status=0
      @purchase.save
    end

    def error(job, exception)
      @purchase.reload
      @purchase.message="Ошибка загрузки из источника: #{exception.to_s}"
      @log.error(exception.to_s) unless @log.nil?
      @log.debug(exception.backtrace) unless @log.nil?
      puts exception.backtrace
      @purchase.save
    end

    def success(job)
      @purchase.reload
      @purchase.message="Данные скачаны из источника успешно"
      @purchase.save
    end
  end
end
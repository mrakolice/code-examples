#encoding:utf-8
require_relative 'worksheet_config'
require 'rubyXL'
require 'spreadsheet'


class Framework::Excel::FileConfig
  def initialize
    @worksheet_configs = []
    @is_spreadsheet = false
  end

  def with_file_name(file_name)
    @file_name = file_name
    self
  end

  def with_worksheet_config(config_func, config_class = Framework::Excel::WorksheetConfig)
    @worksheet_configs.push config_func.call(config_class.new)
    self
  end

  def is_spreadsheet
    @is_spreadsheet = true
    self
  end

  def run
    book = @is_spreadsheet ? Spreadsheet.open(@file_name) : RubyXL::Parser.parse(@file_name)

    @worksheet_configs.map{|c|c.run(book)}.flatten
  end
end
#encoding:utf-8
class Framework::Image
  attr_accessor :path
  attr_accessor :size
  attr_accessor :hash

  def initialize(path, size, hash)
    @path, @size, @hash = path, size, hash
  end

  def to_json
    {'path' => @path, 'size' => @size, 'hash' => @hash}
  end
end
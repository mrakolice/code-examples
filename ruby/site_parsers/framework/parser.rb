
require_relative 'site/product_config'
require_relative 'base_site_job'
require_relative 'file_download_config'
require 'mechanize'
module Framework
  class Parser < Framework::BaseSiteJob
    attr_accessor :is_download_files

    def initialize
      @dlparams = {} if @dlparams.nil?
      @dlparams = {'is_download_files' => :checkbox}.merge!(@dlparams)

      @picture_path=Spup2::Application.config.image_path+self.class.name.gsub('::', '_')+'/'
      FileUtils.mkpath(@picture_path) unless File.exists? @picture_path
      @auth_data = nil
      @category_links_getters = []
      @catalog_urls = []
      @product_config = nil
      @category_func = nil
      @additional_category_links = []
      @file_configs = {}
      @download_files = {}
    end

    def initialize_settings

    end

    ### CORE METHODS
    def process
      download_files if @is_download_files

      @catalog_urls.each { |url| process_catalog(url) }
      @additional_category_links.uniq!
      @additional_category_links.each{|link| process_category(link)}

      raise 'Пустая коллекция товаров' if Purchase.find(purchase_id).collections.empty?
    end

    def download_files
      puts 'start download'
      @file_configs.each do |key, config|
        puts key
        @download_files[key] = config.run
      end
      puts 'end download'
    end

    def process_catalog(catalog_url)
      catalog_page = get_page catalog_url

      category_links = get_category_links(catalog_page)

      category_links.uniq!

      category_links.each { |link| process_category(link) }
    end

    def process_category(link)
      next_link = link
      result = []
      begin
        @log.debug "Process category #{next_link}"
        category_page = get_page next_link

        result.concat process_products(category_page)

        next_link = get_next_link(category_page)
      end until next_link.nil?
      result
    end

    def get_next_link(category_page)
      return nil if @next_link_getter.nil?
      return @next_link_getter.call(category_page) unless @next_link_getter.is_a? String

      next_link_elem = @next_link_getter.nil? ? nil : category_page.search(@next_link_getter).first

      return nil if next_link_elem.nil? or next_link_elem.attr('href').nil? or next_link_elem.attr('href').to_s == '#'

      next_link_elem.attr('href').to_s
    end

    def process_products(category_page)
      @product_config.with_category(->(page) { @category_func.call(category_page) }) unless @category_func.nil?
      @product_config.with_picture_path(@picture_path)
      @product_config.run(@agent, category_page)
    end

    def get_category_links(catalog_page)
      @category_links_getters.map { |getter| getter.is_a?(String) ? get_links(catalog_page, getter) : getter.call(catalog_page) }.flatten
    end

    def get_page(link)
      @agent.get(link)
    end

    ### SETTINGS METHODS
    def with_url(catalog_url)
      @catalog_urls.push catalog_url
      self
    end

    def with_product_config(product_config_func=nil, product_config_class=Framework::Site::ProductConfig)
      @product_config = product_config_class.new
      @product_config = product_config_func.call(@product_config) unless product_config_func.nil?
      @product_config.with_purchase(@purchase)
      @product_config.with_logging(->(msg){@log.debug msg}, ->(msg){@log.error msg})
      self
    end

    def with_category_links(getter, is_link=false)
      if is_link
        @additional_category_links.push getter
      else
        @category_links_getters << getter
      end
      self
    end

    def with_category(category_func)
      @category_func = category_func
      self
    end

    def with_pagination(next_link_getter)
      @next_link_getter = next_link_getter
      self
    end

    def clean
      @purchase.collections.each do |col|
        col.destroy
      end
      @purchase.product_news.each do |prod|
        prod.destroy
      end
    end

    def with_file_download_config(file_name, file_download_config_func, file_download_config_class=Framework::FileDownloadConfig)
      file_config = file_download_config_func.call(file_download_config_class.new)
      file_config.with_folder_path(@file_path)
      @file_configs[file_name] = file_config
      self
    end

  end
end
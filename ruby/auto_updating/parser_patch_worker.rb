#encoding:utf-8

class ParserPatchWorker
  include Sidekiq::Worker

  sidekiq_options retry: false

  def perform(diff, provider_id)
    provider = Provider.find(provider_id)
    service = Parser::ParserPatchService.new(provider)
    service.merge(diff)
  rescue => e
    provider.parser_data
        .update_attributes({
                               :status => 'error_at_upload',
                               :error_message => "#{e.message}; #{e.backtrace}"
                           })
    raise e
  end
end
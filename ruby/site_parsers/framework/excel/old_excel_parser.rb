#encoding:utf-8
require_relative 'file_config'
require_relative '../validator'
require 'rubyXL'

module Framework
  module Excel
    class OldExcelParser
      def initialize
        @file_configs = []
      end

      def with_file_config(file_config_func, file_config_class = Framework::Excel::FileConfig)
        @file_configs.push file_config_func.call(file_config_class.new)
        self
      end

      def with_validator(validator_class=Framework::Validator, validator_func=nil)
        if validator_func.nil?
          @validator = validator_class.new
        else
          @validator = validator_func.call(validator_class.new)
        end
        self
      end

      def run
        products = []
        products = @file_configs.map{|config| config.run}.flatten

        puts "Products length #{products.length}"

        products if @validator.nil? or @validator.validate_all products
      end
    end
  end
end
#encoding:utf-8

module Framework
  module Site
    class AuthorizationData
      attr_accessor :url
      attr_accessor :login
      attr_accessor :password
      attr_accessor :form_func
      attr_accessor :login_field_name
      attr_accessor :password_field_name

      def initialize
        @btn_func = ->(form){form.buttons.first}
      end

      def run(agent)
        auth_page = agent.get @url
        auth_page.encoding = 'utf-8'
        login_form = @form_func.call(auth_page)
        login_form[@login_field_name] = @login
        login_form[@password_field_name] = @password
        agent.submit(login_form, @btn_func.call(login_form))
      end

      def with_submit_button(btn_func)
        @btn_func = btn_func
        self
      end

      def with_auth_url(url)
        self.url = url
        self
      end

      def with_login(login)
        self.login = login
        self
      end

      def with_password(password)
        self.password = password
        self
      end

      def with_login_field_name(login_field_name)
        self.login_field_name = login_field_name
        self
      end

      def with_password_field_name(password_field_name)
        self.password_field_name = password_field_name
        self
      end

      def with_form(form_func)
        self.form_func = form_func
        self
      end


    end
  end
end


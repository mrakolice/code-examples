#encoding:utf-8

require_relative '../framework/parser'
require_relative 'complex_example_product_config'
require 'htmlentities'

module Bimki
  class BimkiParser < Framework::Parser
    def initialize_settings
      with_url('http://ComplexExample.ru/')
          .with_category_links('.ty-menu__submenu-link')
          .with_category_links('/rasprodaja.php/', true)
          .with_category_links('/novinki/', true)
          .with_pagination('.ty-pagination__next[href]')
          .with_product_config(->(pc) {
            pc.with_product_selector(->(category_page) {
              category_page.search('.ty-grid-list__image a').to_a.uniq { |a| a.attr('href').to_s }
            })
                .with_category(->(product_page) {
                  parent = prepare_string product_page.search('.ty-menu__item-active > a.ty-menu__item-link').first.text
                  header = product_page.search('.ty-menu__submenu-item-header-active > a').first
                  header = (header.nil? ? '' : prepare_string(header.text))
                  last = product_page.search('.ty-menu__submenu-item-active > a').first
                  last = (last.nil? ? '' : prepare_string(last.text))

                  return prepare_string "#{parent}, #{header}, #{last}" unless header.blank?
                  return prepare_string "#{parent}, #{last}" unless last.blank?
                  prepare_string parent
                })
                .with_articul(->(product_page) {
                  result = prepare_string product_page.search('meta[itemprop=sku]').first.attr('content').to_s

                  if result.blank?
                    full_name = prepare_string product_page.search('meta[itemprop=name]').first.attr('content').to_s

                    parts = full_name.split

                    result = parts[0]
                    result = parts[1] if parts[0].mb_chars.downcase.to_s.include? 'комплект'
                  end

                  prepare_string result
                })
                .with_name(->(product_page) {
                  full_name = prepare_string product_page.search('meta[itemprop=name]').first.attr('content').to_s

                  parts = full_name.split

                  name = parts[1]
                  name = parts[0] if parts[0].mb_chars.downcase.to_s.include? 'комплект'

                  return prepare_string(name) unless name.blank?

                  full_name
                })
                .with_images(->(product_page) {
                  product_page.search('.ty-product-img > a').map { |a| a.attr('href').to_s }.uniq
                })
                .with_image_config(->(ic) {
                  ic.with_default_url('http://ComplexExample.ru/')
                })
                .with_description(->(product_page) {
                  inner = product_page.search('.content-description > div').inner_html

                  prepare_string(HTMLEntities.new.decode(inner.gsub(/<\/?.+?\/?>/, '').gsub(/\. \./, '.').gsub('..', '.').gsub(',.', ',')))
                })
                .with_price(->(product_page) {
                  ComplexExample::Utilities.get_price(product_page)
                })
                .with_sizes(->(product_page) {
                  []
                })
                .with_composition(->(product_page) {
                  value = product_page.search('.ty-product-feature__value').first

                  return prepare_string value.text unless value.nil?
                  ''
                })
                .with_category_type('544')
                .with_category_type('1379', 'школьная', 'рюкзаки')
                .with_category_type('1377', 'школьная', 'девочкам')
                .with_category_type('1386', 'школьная', 'мальчикам')
                .with_category_type('528', 'спортивная', 'девочкам', 'брюки')
                .with_category_type('962', 'спортивная', 'девочкам', 'костюмы')
                .with_category_type('964', 'спортивная', 'девочкам', 'футболки')
                .with_category_type('963', 'спортивная', 'девочкам', 'шорты')
                .with_category_type('528', 'спортивная', 'мальчикам', 'брюки')
                .with_category_type('962', 'спортивная', 'мальчикам', 'костюмы')
                .with_category_type('964', 'спортивная', 'мальчикам', 'футболки')
                .with_category_type('963', 'спортивная', 'мальчикам', 'шорты')
                .with_category_type('961', 'спортивная', 'мальчикам', 'джемперы')
                .with_category_type('559', 'девочкам', 'белье')
                .with_category_type('546', 'девочкам', 'блузки')
                .with_category_type('4680', 'девочкам', 'бриджи')
                .with_category_type('4680', 'девочкам', 'брюки')
                .with_category_type('2448', 'девочкам', 'водолазки')
                .with_category_type('1298', 'девочкам', 'шапки')
                .with_category_type('553', 'девочкам', 'джемперы')
                .with_category_type('550', 'девочкам', 'жакеты')
                .with_category_type('1377', 'девочкам', 'жилеты')
                .with_category_type('547', 'девочкам', 'куртки')
                .with_category_type('559', 'девочкам', 'носки')
                .with_category_type('551', 'девочкам', 'пижамы')
                .with_category_type('552', 'девочкам', 'платья')
                .with_category_type('552', 'девочкам', 'сарафан')
                .with_category_type('546', 'девочкам', 'туники')
                .with_category_type('556', 'девочкам', 'футболки')
                .with_category_type('557', 'девочкам', 'шорты')
                .with_category_type('558', 'девочкам', 'юбки')
                .with_category_type('847', 'мальчикам', 'белье')
                .with_category_type('561', 'мальчикам', 'бриджи')
                .with_category_type('561', 'мальчикам', 'брюки')
                .with_category_type('2449', 'мальчикам', 'водолазки')
                .with_category_type('1298', 'мальчикам', 'головные')
                .with_category_type('565', 'мальчикам', 'джемперы')
                .with_category_type('562', 'мальчикам', 'жилеты')
                .with_category_type('563', 'мальчикам', 'комбинезоны')
                .with_category_type('560', 'мальчикам', 'куртки')
                .with_category_type('570', 'мальчикам', 'носки')
                .with_category_type('564', 'мальчикам', 'пижамы')
                .with_category_type('566', 'мальчикам', 'рубашки')
                .with_category_type('568', 'мальчикам', 'футболки')
                .with_category_type('569', 'мальчикам', 'шорты')
                .with_category_type('572', 'малышам', 'боди')
                .with_category_type('571', 'малышам', 'трикотаж')
                .with_category_type('573', 'малышам', 'комбинезоны')
                .with_category_type('577', 'малышам', 'кофты')
                .with_category_type('539', 'малышам', 'майки')
                .with_category_type('576', 'малышам', 'носки')
                .with_category_type('575', 'малышам', 'платья')
                .with_category_type('574', 'малышам', 'ползунки')
                .with_category_type('539', 'малышам', 'распашонки')
                .with_category_type('578', 'малышам', 'штанишки')
                .with_category_type('546', 'нарядная', 'блузки')
                .with_category_type('550', 'нарядная', 'болеро')
                .with_category_type('552', 'нарядная', 'платья')
                .with_category_type('558', 'нарядная', 'юбки')
                .with_category_type('979', 'аксессуары', 'бижутерия')
                .with_category_type('978', 'аксессуары', 'перчатки')
                .with_category_type('845', 'аксессуары', 'уборы')
                .with_category_type('151', 'аксессуары', 'малышам')
                .with_category_type('1405', 'аксессуары', 'галстуки')
                .with_category_type('1405', 'аксессуары')
                .with_category_type_by_name('535', 'туфли')
                .with_category_type_by_name('1447', 'чешки')
                .with_category_type_by_name('862', 'сланцы')
                .with_category_type_by_name('584', 'ботинки')
                .with_category_type_by_name('537', 'сапоги')

          }, ComplexExample::ComplexExampleProductConfig)
    end

    def get_page(link)
      begin
        @agent.get(link)
      rescue Mechanize::ResponseCodeError => exception
        if exception.response_code == '404'
          return nil
        else
          raise # Some other error, re-raise
        end
      end
    end

    def process_category(link)
      page = get_page link
      return if page.search('.ty-subcategories__item').length > 0
      super(link)
    end
  end
end
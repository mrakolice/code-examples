#encoding:utf-8
require_relative '../parser'
require_relative 'excel_product_config'
require 'spreadsheet'

module Framework
  module Site
    class ExcelParser < Framework::Parser

      attr_accessor :price_file

      def initialize
        @dlparams = {} if @dlparams.nil?
        @dlparams={'price_file' => :file}.merge!(@dlparams)
        super
      end

      def process
        products = parse_files
        @product_config.with_picture_path(@picture_path).with_agent(@agent)

        products.each do |product|
          @product_config.add_product product
        end
      end

      def parse_files
        []
      end

      def with_product_config(product_config_func=nil, product_config_class=Framework::Site::ExcelProductConfig)
        super(product_config_func, product_config_class)
      end
    end
  end
end
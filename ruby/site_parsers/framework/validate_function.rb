class Framework::ValidateFunction
  attr_accessor :func
  attr_accessor :msg

  def initialize(func, msg)
    self.func = func
    self.msg = msg
  end
end

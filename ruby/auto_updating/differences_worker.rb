#encoding:utf-8

class DifferencesWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(parser_data_id)
    service = Parser::ParserService.new(ParserData.find(parser_data_id))
    service.get_difference
  end
end
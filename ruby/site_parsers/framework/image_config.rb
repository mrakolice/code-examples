#encoding:utf-8
require_relative 'image'
class Framework::ImageConfig
  def initialize
    @with_logging = false
    @log_func = ->(msg) { puts msg }
    @error_log_func = ->(msg) { puts msg }
  end

  def run(url, main_name, index)
    url = "#{@default_url}/#{url}" unless url.include? 'http://' or url.include? 'https://'
    name = "#{main_name.gsub('/', '_')}~#{index}"
    save_picture(url, name, true)
    path = "#{@picture_path}#{name}.jpg"
    return nil unless File.exists? path

    hash = Digest::MD5.hexdigest(File.read(path))
    size = File.size(path)
    Framework::Image.new(name, size, hash)
  end

  def save_picture(url, name, overwrite, scale=true, local=false)
    copy_to=[]
    if name.is_a? Array
      copy_to=name[1..-1]
      name=name[0]
    end

    fname="#{@picture_path}#{name}.jpg"

    @log_func.call "Downloading #{url} to #{fname}"

    unless overwrite
      return false if File.exists? fname
    end
    #@picThreads<<Thread.new { @agent.download(url,fname) }
    begin
      if local
        FileUtils.cp(url, fname)
      else
        download_picture(url, fname)

        copy_to.each do |f2|
          fname2="#{@picture_path}#{f2}.jpg"
          FileUtils.cp(fname, fname2)
        end
      end
    rescue Mechanize::Error
      puts "Error downloading #{url}"
      @error_log_func.call "Error downloading #{url}"
      return true
    end
    begin
      img = Magick::Image::read(fname).first

      if img.format=='PNG'
        img_list = Magick::ImageList.new
        img_list.read(fname)
        img_list.new_image(img_list.first.columns, img_list.first.rows) { self.background_color = "white" }
        img = img_list.reverse.flatten_images
      end

      img.strip!
      img.change_geometry!('1600x1600>') { |cols, rows, img|
        img.resize!(cols, rows)
      }

      @img_proc_func.call(img) unless @img_proc_func.nil?

      img.write(fname) { self.quality = 75 }
    rescue Magick::ImageMagickError => e
      @error_log_func.call "Error processing image, removed #{url}, #{fname}"
      @error_log_func.call e.to_s
      @error_log_func.call e.backtrace
    end if scale
  end

  def download_picture(url, fname)
    @agent.download(url, fname)
  end

  def with_default_url(default_url)
    @default_url = default_url
    self
  end

  def with_agent(agent)
    @agent = agent
    self
  end

  def with_picture_path(picture_path)
    @picture_path = picture_path
    self
  end

  def with_logging(log_func = nil, error_log_func = nil)
    @log_func = log_func unless log_func.nil?
    @error_log_func = error_log_func unless error_log_func.nil?
    @with_logging = true
    self
  end

  def with_img_proc(img_proc_func)
    @img_proc_func = img_proc_func
    self
  end
end
class String
  def is_number?
    true if Float(self) rescue false
  end
end

def get_price(price, coefficient, is_retail=false)
  result = (price * coefficient).ceil

  # Округляем до 5 до предыдущей сотни
  mod = 5
  m = result % mod
  if m > 0
    result = result - m

    result = result + mod if (result + mod)/100 == result/100
  end
  add_price = get_add_price(result)

  return result + add_price if is_retail

  if result < 1000
    return result + add_price if (result + add_price)/100 == result/100

    result = (result/100*100 + 95) if (result + add_price)/100 > result/100
    return result
  end

  result = result + add_price if (result + add_price)/1000 <= result/1000
  result = (result/1000*1000 + 995) if (result + add_price)/1000 > result/1000

  result
  # 400 -> 420
  # 480 -> 490
  # 3810 -> 3990
  # 3800 -> 3990
  # 3910 -> 3990
  # 3990 -> 3990
end

def get_add_price(price)
  # до 500 руб текущая (!) цена на сайте у нас повысить цены на 20 руб,
  # от 500 до 1000 на 60,
  # от 1000 до 2000 на 100,
  # от 2000 до 3000 на 130,
  # свыше 3000 на 190
  return 20 if price < 500
  return 60 if price < 1000
  return 100 if price < 2000
  return 130 if price < 3000
  190
end

def get_links(page, selector)
  page.search(selector).map{|a|a.attr('href').to_s}
end

def get_images(elem, selector)
  elem.search(selector).map{|img|img.attr('src').to_s}
end

# Обрабатываем строку (убираем невидимые символы, лишние пробелы и прочая)
def prepare_string(arg_str)
  return if arg_str.nil?
  str = arg_str
  # Обрабатывает строку и убирает всякие двойные пробелы, табуляции и прочие вещи

  str.gsub!("\t", ' ') # иначе можно посклеивать слова, где они только табом разделены
  str.gsub!(/ +/,' ') # регэкспы наши большие друзья при обработке текста

  str.strip
end

class Object; def ensure_array; [self] end end
class Array; def ensure_array; to_a end end
class NilClass; def ensure_array; to_a end end
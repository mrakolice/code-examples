#encoding:utf-8

module Parser
  class DiffData
    attr_accessor :folder_title
    attr_accessor :folder_field
    attr_accessor :product_sku
    attr_accessor :product_field
    attr_accessor :color_name
    attr_accessor :color_field
    attr_accessor :inner_field
    attr_accessor :deep_inner_field
    attr_accessor :sign
    attr_accessor :result_object

    attr_accessor :is_folder
    attr_accessor :is_product
    attr_accessor :is_color
    attr_accessor :is_sizes
    attr_accessor :is_pics
    attr_accessor :is_color_image

    def initialize(diff_path, sign, result_object)
      # inner_field - наименование размера/хэш картинки/поле из color_image
      # deep_inner_field - наименование поля в размере/поля в sizes/pics
      @sign = sign
      @result_object = result_object
      path = HashDiff.decode_property_path(diff_path, DiffData.delimiter)
      path.each_with_index do |element, i|
        path[i] = element.mb_chars.to_s
      end

      @folder_title, @folder_field, @product_sku, @product_field, @color_name, @color_field, @inner_field, @deep_inner_field = path

      @inner_field = '-' if @inner_field == 'default'
      @color_name = '-' if @color_name == 'default'

      @is_folder = (path.length == 1)
      @is_product = (path.length == 3 or path.length == 4)
      @is_color = (@product_field == 'color')
      @is_sizes = (@color_field == 'sizes')
      @is_pics = (@color_field == 'pics')
      @is_color_image = (@color_field == 'color_image')
    end

    def get_symbol
      if @is_folder
        return :folder
      elsif @is_product
        return :product
      elsif @is_color
        if @is_sizes
          return :sizes
        elsif @is_pics
          return :pics
        elsif @is_color_image
          return :color_image
        else
          return :color
        end
      end
    end

    def get_path(*args)
      color_name = (@color_name == '-') ? 'default' : @color_name
      inner_field = (@inner_field == '-') ? 'default' : @inner_field
      array = [@folder_field, @product_sku, @product_field, color_name, @color_field, inner_field, @deep_inner_field]
      result = @folder_title
      array.each do |value|
        break if value.nil?
        result = "#{result}#{DiffData.delimiter}#{value}"
      end
      args.each do |value|
        result = "#{result}#{DiffData.delimiter}#{value}"
      end
      result
    end

    def self.delimiter
      '/./'
    end

  end
end
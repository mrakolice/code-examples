#encoding:utf-8

require_relative 'validate_function'

class Framework::Validator
  def initialize
    @error_count = 0
    @validate_functions = []
    @log_func = ->(msg){puts msg}
    @error_log_func = ->(msg){puts msg}
  end

  def add_function(callback, msg)
    @validate_functions.push(Framework::ValidateFunction.new(callback, msg))
    self
  end

  def validate(product)
    @validate_functions.each do |func|
      if func.func.call(product)
        @error_log_func.call func.msg unless func.msg.nil?
        @error_log_func.call 'Validation Error' if func.msg.nil?
        log_product product
        return "Validation error: #{func.msg}. Articul: #{product.articul}"
      end
    end
    nil
  end

  def with_logging(log_func=nil, error_log_func=nil)
    @log_func = log_func unless log_func.nil?
    @error_log_func = error_log_func unless error_log_func.nil?
    self
  end
  def log_product(product)
    @log_func.call 'LOG PRODUCT'
    @log_func.call "Category:#{product.category}"
    @log_func.call "Name:#{product.name}"
    @log_func.call "Articul:#{product.articul}"
    @log_func.call "Price:#{product.price}"
    @log_func.call "Description:#{product.description}"
    if product.images.nil? or not product.images.length
      @log_func.call 'Images length 0'
    else
      @log_func.call "Images length:#{product.images.length}"
      product.images.each { |i| puts i }
    end
    if product.is_a? Framework::SizeableParsedProduct
      if product.sizes.nil? or not product.sizes.length
        @log_func.call 'Sizes length 0'
      else
        puts "Sizes length:#{product.sizes.length}"
        product.sizes.each { |i| @log_func.call i }
      end
    end
    @log_func.call 'END LOG PRODUCT'
  end
end

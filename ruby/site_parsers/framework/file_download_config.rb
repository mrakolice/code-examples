#encoding:utf-8
require_relative 'site/authorization_data'

module Framework
  class FileDownloadConfig

    def with_auth_data(auth_data_func)
      @auth_data = auth_data_func.call(Framework::Site::AuthorizationData.new)
      self
    end

    def run
      create_agent

      authorize

      process
    end

    def authorize
      @auth_data.run(@agent) unless @auth_data.nil?
    end

    def with_download_url(download_url)
      @download_url = download_url
      self
    end

    def with_file_name(file_name)
      @file_name = file_name
      self
    end

    def with_folder_path(folder_path)
      @folder_path = folder_path
      self
    end

    def create_agent
      @agent = Mechanize.new
      @agent.user_agent = 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.94 Safari/537.36'
      @agent.pluggable_parser.default = Mechanize::Download
      @agent
    end

    def download
      file_name = File.join @folder_path, @file_name
      puts file_name, @download_url
      save_file(file_name)
    end

    def save_file(file_name)
      @agent.get(@download_url).save(file_name)
    end

    def process
      download
    end
  end
end

#encoding:utf-8

module ComplexExample
  class Utilities

    def self.get_security_hash(product_page)
      product_page.body.match(/_\.security_hash\s*=\s*'(.+)'/).captures[0]
    end

    def self.get_price(product_page)
      (product_page.search('meta[itemprop=price]').first.attr('content').to_s.to_i * 1.05).ceil
    end

    def self.get_size_div(product_page)
      div=get_option_div(product_page,'Выберите размер :')
      div=get_option_div(product_page,'Выберите размер:') if div.nil?
      div=get_option_div(product_page,'Выберите рост :') if div.nil?
      div=get_option_div(product_page,'Выберите рост:') if div.nil?
      div
    end

    def self.get_color_div(product_page)
      div=get_option_div(product_page,'Выберите цвет :')
      div=get_option_div(product_page,'Выберите цвет:') if div.nil?
      div
    end

    def self.get_option_div(product_page,label)

      options={}

      product_page.search('label.ty-product-options__item-label').each { |l|
        label_text = prepare_string l.text
        div=l.parent.search('.color-box-variants')
        options[label_text]=div
      }

      options[label]
    end

    def self.get_available_colors(product_page)

      size_div=get_size_div(product_page)
      color_div=get_color_div(product_page)
      return nil if color_div.nil?

      colors = {}

      size_elements=[]
      unless size_div.nil?
        size_elements = size_div.search('.option-items').map { |lbl| {
            'name' => lbl.search('input').attr('name').to_s,
            'value' => lbl.search('input').attr('value').to_s.to_i,
            'text' => prepare_string(lbl.text),
            'disabled' => lbl.attr('class').include?('disabled')
        } }
      end

      color_div.search('.option-items').each { |lbl|
        name = prepare_string lbl.text
        colors[name] = {'sizes' => []}
      }

      product_id = product_page.search("[name*='product_data']").first.attr('value').to_s

      security_hash = get_security_hash(product_page)

      size_elements.each do |size_element|
        begin
          page_with_sizes = get_page_with_options(size_element['name'], size_element['value'], security_hash, product_id)

          color_div2=get_color_div(page_with_sizes)
          available_colors = color_div2.search('.option-items:not(.disabled)').map { |lbl|
            {
                'name' => lbl.search('input').attr('name').to_s,
                'value' => lbl.search('input').attr('value').to_s.to_i,
                'text' => prepare_string(lbl.text)
            } }

          available_colors.each do |color|
            if colors.include? color['text']
              next if size_element['disabled']
              colors[color['text']]['sizes'] << size_element
              colors[color['text']]['color'] = color if colors[color['text']]['color'].nil?
              next
            end
            colors[color['text']] = {'color' => color}
            page_with_colors = get_page_with_options(color['name'], color['value'], security_hash, product_id)

            size_div2=get_size_div(page_with_colors)
            colors[color['text']]['sizes'] = size_div2.search('.option-items:not(.disabled)').map { |lbl| {
                'name' => lbl.search('input').attr('name').to_s,
                'value' => lbl.search('input').attr('value').to_s.to_i,
                'text' => prepare_string(lbl.text)
            } }

          end

        rescue Mechanize::ResponseCodeError => exception
          puts "ERROR: #{exception.to_s}"
        end

      end

      colors.each do |color, sizes|
        colors[color]['sizes'].uniq!{|s| s['text']}
        colors[color]['sizes'].sort_by!{|s| s['text'].to_i}
      end

      colors
    end


    def self.get_colors(product_page)
      color_div=get_color_div(product_page)
      size_div=get_size_div(product_page)

      return nil if color_div.nil?

      colors = {}

      size_elements=[]
      unless size_div.nil?
        size_elements = size_div.search('.option-items').map { |lbl| {
            'name' => lbl.search('input').attr('name').to_s,
            'value' => lbl.search('input').attr('value').to_s.to_i,
            'text' => prepare_string(lbl.text)
        } }
      end


      color_div.search('.option-items').each { |lbl|
        name = prepare_string lbl.text
        colors[name] = []
      }

      product_id = product_page.search("[name*='product_data']").first.attr('value').to_s

      security_hash = get_security_hash(product_page)

      size_elements.each do |size_element|
        begin
          page_with_sizes = get_page_with_options(size_element['name'], size_element['value'], security_hash, product_id)

          color_div2=get_color_div(page_with_sizes)

          available_colors = color_div2.search('.option-items:not(.disabled)').map { |lbl|
            {
                'name' => lbl.search('input').attr('name').to_s,
                'value' => lbl.search('input').attr('value').to_s.to_i,
                'text' => prepare_string(lbl.text)
            } }

          available_colors.each do |color|
            if colors.include? color['text']
              colors[color['text']] << size_element['text']
              next
            end
            page_with_colors = get_page_with_options(color['name'], color['value'], security_hash, product_id)
            size_div2=get_size_div(page_with_colors)

            colors[color['text']] = size_div2.search('.option-items').map{|lbl| prepare_string lbl.text}

          end

        rescue Mechanize::ResponseCodeError => exception
          puts "ERROR: #{exception.to_s}"
        end

      end

      colors.each do |color, sizes|
        colors[color].uniq!
        colors[color].sort_by!{|s| s.to_i}
      end

      colors
    end

    def self.get_page_with_options(option_name, option_value, security_hash, product_id)
      ajax_json = {
          'dispatch' => 'products.options',
          option_name => option_value,
          'appearance[show_product_options]' => 1,
          'security_hash' => security_hash,
          'is_ajax' => 1,
          'result_ids' => "product_options_update_#{product_id}",
      }

      puts ajax_json

      json_result = Mechanize.new.post('http://ComplexExample.ru', ajax_json)
      puts json_result.content

      json_result = JSON.parse(json_result.content)

      Nokogiri::HTML(json_result['html'].values.first)
    end

  end
end
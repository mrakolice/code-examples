#encoding:utf-8
require_relative 'validator'

class Framework::ProductValidator < Framework::Validator

  def initialize
    super
    add_function ->(product) { product.category.blank? }, 'ERROR: Category is empty'
    add_function ->(product) { product.articul.blank? }, 'ERROR: Articul is empty'
    add_function ->(product) { product.images.nil? or product.images.length == 0 }, 'ERROR: Images is empty'
    add_function ->(product) { product.name.blank? }, 'ERROR: Name is empty'
    add_function ->(product) { product.price.nil? }, 'ERROR: Price is empty'
    add_function ->(product) { product.price == 0 }, 'ERROR: Price is equal 0'
    add_function ->(product) { product.price.is_a? String }, 'ERROR: Price is not a number'
  end
end

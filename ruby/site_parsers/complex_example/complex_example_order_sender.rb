#encoding:utf-8

require_relative '../framework/order_sender'
require_relative 'utilities'

module ComplexExample
  class ComplexExampleOrderSender < Framework::OrderSender

    def initialize
      # @dlparams = {'agent_header' => :text}
      super
      @need_agent_header = true
    end

    def clean
      get_page 'http://ComplexExample.ru/index.php?dispatch=checkout.clear'
      
      #urls = page.search('[data-ca-dispatch]').map { |a| a.attr('href').to_s }

      #urls.each { |url| get_page(url) }
    end

    def get_page(link)
      sleep(rand(0.4) + 0.3)
      @agent.get link
    rescue Mechanize::ResponseCodeError => exception
      if exception.response_code == '404'
        return nil
      elsif exception.response_code == '503'
        retry
      else
        raise
      end
    end

    def initialize_settings
      puts @agent_header
      @agent.user_agent = @agent_header
      with_auth_data(->(ad){
        ad.with_auth_url('http://ComplexExample.ru/login')
            .with_login('ComplexExample')
            .with_password('ComplexExample')
            .with_login_field_name('user_login')
            .with_password_field_name('password')
            .with_form(->(page) {
              page.forms[1]
            })
      })
    end

    def order(articul, colors)
      debug_func(articul)
      debug_func( colors)
      search_page = get_page('http://ComplexExample/?search_performed=Y&dispatch=products.search&q='+articul)

      links = search_page.search('.ty-grid-list__image a')

      if links.length == 0
        add_error_csv_data('Cannot find product', articul, colors)
        return
      end

      link = links[0].attr('href').to_s
      product_page = get_page(link)

      page_art=product_page.search('meta[itemprop=sku]').first.attr('content').to_s.strip

      if page_art!=articul
        add_error_csv_data('Articul mismatch', articul, colors)
        return
      end

      security_hash = ComplexExample::Utilities.get_security_hash(product_page)

      # first - product id. need to check with regex \[\d+\]

      product_data = product_page.search('[type=hidden][name*=product_data]').to_a
                         .find { |input| not input.attr('name').match(/\[\d+\]/).nil? }

      if product_data.nil?
        add_error_csv_data('Cannot find product_id', articul, colors)
        return
      end

      available_colors = ComplexExample::Utilities.get_available_colors(product_page)

      debug_func available_colors

      if available_colors.nil?
        add_error_csv_data('Cannot find any kind of color', articul, colors)
        return
      end

      actual_price = ComplexExample::Utilities.get_price(product_page)

      colors.each do |color, sizes|
        debug_func color

        available_color = available_colors[color]

        debug_func available_color

        if available_color.nil?
          add_error_csv_data("Color #{color} not available", articul, color)
          next
        end

        sizes.each do |size, obj|
          debug_func size

          if (obj['price'] * 1.1).ceil < actual_price
            add_error_csv_data("Price difference is more than 10% for #{articul}, #{size}", articul, color, size, obj['quantity'])
            next
          end

          available_size = available_color['sizes'].find {|s| s['text'].end_with? "/#{size}"}

          available_size = available_color['sizes'].find {|s| s['text'] == size} if available_size.nil?

          if available_size.nil?
            add_error_csv_data("Cannot find size #{size}", articul, color, size, obj['quantity'])
            next
          end

          debug_func available_size

          json = {
              'result_ids'=>'cart_status*,wish_list*,checkout*,account_info*',
              product_data.attr('name') => product_data.attr('value'),
              available_size['name'] => available_size['value'],
              available_color['color']['name'] => available_color['color']['value'],
              'security_hash' => security_hash,
              "product_data[#{product_data.attr('value')}][amount]" => obj['quantity'],
              'dispatch[checkout.add]' => '',
              'is_ajax' => 1
          }

          puts json

          sleep(rand(0.9) + 0.4)
          result = @agent.post('http://ComplexExample.ru', json)
          #puts result.content.to_s
          puts @agent.cookies

        end
      end
    end

    def add_csv_data(fields)
      articul = fields['Товар'].to_s
      size = fields['Размер'].to_s
      price = fields['Цена'].to_i
      color_description = fields['Подробнее'].to_s.match(/Цвет: (.+?)\./).captures[0]
      color_name = prepare_string fields['Наименование'].to_s.split('Цвет: ').last

      color = color_name.length > color_description.length ? color_name : color_description

      @csv_data[articul] = {} unless @csv_data.include? articul

      @csv_data[articul][color] = {} unless @csv_data[articul].include? color

      @csv_data[articul][color][size]['quantity']+=1 if @csv_data[articul][color].include? size
      @csv_data[articul][color][size] = {'quantity' => 1} unless @csv_data[articul][color].include? size

      @csv_data[articul][color][size]['price'] = price
    end
  end
end
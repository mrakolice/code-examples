#encoding:utf-8
module Parser
  class ParserService
    def initialize(parser_data = nil)
      @parser_data = parser_data
      @agent = Mechanize.new
      authorize
    end

    def authorize
      @agent.post ENV['AUTH_URI'], {
          ENV['LOGIN_FIELD_NAME'] => ENV['LOGIN'],
          ENV['PASSWORD_FIELD_NAME'] => ENV['PASSWORD'],
      }
    end

    def get_folder_data
      return {} if @parser_data.parser_id == -1 or @parser_data.parser_id.nil?

      JSON.parse @agent.get(_get_uri('GET_PURCHASE_DATA')).content
    end

    def get_difference
      # Качаем данные
      # Бежим по ним и сравниваем с теми, что есть
      source_folders = _get_parser_sources
      puts 'finished get_parser_sources'
      folders = _get_inner_folders_structure
      puts 'finished get_inner_folders'

      diff = HashDiff.diff(folders, source_folders, :delimiter => DiffData.delimiter, :strict => false) do |path, obj1, obj2|
        # Из-за логики, что блок должен возвращать true\false.
        # Если nil, то сравнение идет методами по умолчанию.
        # Для того, чтобы видеть, какие картинки удалились\добавились
        case path
          when /color_image/, /#{DiffData.delimiter}pics#{DiffData.delimiter}/
            _compare_pics(obj1, obj2)
          when /sizes_table/
            _compare_sizes_table(obj1, obj2)
          when /cat_type/
            _compare_category_types(obj1, obj2)
          when /additional/
            _compare_additional(obj1, obj2)
        end
      end
      puts 'finished get_diff'
      folders = nil
      source_folders = nil
      ParserPatchWorker.perform_async(diff, @parser_data.provider.id)
      diff = nil
    end

    def start_parse(user_id)
      sources = JSON.parse @agent.get(_get_uri('GET_PRODUCTS')).content

      ParserWorker.perform_async sources, user_id, @parser_data.id, false
    end

    def start_download
      @agent.post _get_uri('START_DOWNLOAD_URI'), {'is_download_files' => true}
      @parser_data.update_attribute(:status, 'parsing')
    end

    private

    def _get_promotion
      @parser_data.provider.promotions.last
    end

    def _compare_pics(obj1, obj2)
      return nil if obj1.nil? or obj2.nil?
      obj1['hash'] == obj2['hash']
    end

    def _compare_additional(obj1, obj2)
      return nil if obj1.nil? or obj2.nil?
      obj1.each{|k,v| return false if not obj2.key?(k) or obj2[k] != v}
      true
    end

    def _compare_sizes_table(obj1, obj2)
      return false if obj1.length != obj2.length

      obj1.each_with_index do |array, i|
        return false if array.length != obj2[i].length

        array.each_with_index do |value, j|
          return false if value != obj2[i][j]
        end
      end
      true
    end

    def _compare_category_types(obj1, obj2)
      return false if obj1.length != obj2.length
      obj1.each_with_index do |value, i|
        return false if value != obj2[i]
      end
      true
    end

    def _get_parser_sources
      sources = JSON.parse @agent.get(_get_uri('GET_PRODUCT_REMAINS')).content

      sources
    end

    def _get_inner_folders_structure
      # Последняя акция, чтобы было к чему прикреплять папки и прочая
      promotion = _get_promotion

      if promotion.nil?
        return {}
      end

      folders = {}
      promotion.folders.each do |folder|
        folders[folder.title]={'products' => {}} unless folders.has_key? folder.title

        folder.products.each do |product|
          product_hash = {
              'sku' => product.sku,
              'title'=> product.title,
              'description'=> product.description,
              'additional'=> JSON.parse(product.additional.gsub('=>', ':')),
              'sizes_table'=> product.sizes_table,
              'sizes_link'=> product.sizes_link,
              'producer'=> product.producer,
              'composition'=> product.composition,
              'cat_type' => _get_category_type(product)
          }

          colors = _get_colors(product)

          product_hash['color'] = colors
          folders[folder.title]['products'][product_hash['sku']] = product_hash
        end
      end

      folders
    end

    def _get_category_type(product)
      result = []
      category = product.category
      return result if product.category.nil?

      begin
        result.insert(0, category.title)
        category = category.parent
      end until category.nil?

      result
    end

    def _get_colors(product)
      colors = {}
      product.colors.each do |color|
        color_hash = {'sizes'=> {}, 'pics'=>{}}
        color.sizes.each do |size|
          color_hash['sizes'][_get_hash_key(size.title)] = {
              'weight'=> size.weight.to_f,
              'price'=> size.price.to_i,
              'retail_price'=> size.retail_price.to_i,
              'stock_price'=> size.stock_price.to_i
          }
        end

        color.images.each do |image|
          color_hash['color_image'] = {'hash'=> image.img_hash, 'size'=> image.file_file_size} if image.color_image
          color_hash['pics'][image.img_hash] = {'hash'=> image.img_hash, 'size'=> image.file_file_size} unless image.color_image
        end

        colors[_get_hash_key(color.title)] = color_hash
      end
      colors
    end

    def _get_hash_key(key)
      (key.blank? or key == '-') ? 'default' : key
    end


    def _get_uri(key)
      "#{ENV['PURCHASES_URI']}/#{@parser_data.parser_id}/#{ENV[key]}"
    end

  end
end
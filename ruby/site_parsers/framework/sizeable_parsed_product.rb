#encoding:utf-8
require_relative 'parsed_product'

class Framework::SizeableParsedProduct < Framework::ParsedProduct
  attr_accessor :sizes
  attr_accessor :sizes_table

  def initialize(category, articul, images, name, description, price, sizes, link=nil, additional={})
    super(category, articul, images, name, description, price, link, additional)
    @sizes = sizes.map(&:clone) || Array.new
    @sizes_table = []
  end

  def get_json_sizes
    result = {}
    @sizes.each{|s|result[s] = {'price'=>get_price(@price, @markup_coefficient), 'stock_price' => @price, 'retail_price' => get_price(@price, @retail_coefficient, true), 'weight'=>0.2}}
    if @sizes.length == 0
      return super
    end
    result
  end
end
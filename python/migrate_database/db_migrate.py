# -*- coding: utf-8 -*-
import logging
from django.forms.models import model_to_dict

import common.models
import bparser.beeline.models
import kassa.models
import bparser.models
import ik.models
import issa.models
import mailer.models
import billing.models
from billing.models import Account
from loc.logger import set_logger
from old_models.models import Accounts, Admins, BeelineChargesforperiodorder, Kassa, BeelineChargesorder, Bills, Block, \
    Users, Tp, Groups, BparserParserdata, ChargeHistory, DelayedBlocks, Details, Statements, Dfactors, IkAbonentuser, \
    IkAbonentnotification, IkOauthtoken, IssaCalendar, IssaParsersession, IssaSearch, KassaAccountvirtualmanagermts, \
    KassaMailersettings, KassaSummarybillstatistic, Log, MailerBox, MailerMessagesettings, Mails, Operations, \
    PaymentHistory, Payments, Queue, Services, Shedule, Transactions, Turns, Updates


class Server(object):
    name = ''

class DbMigrator(object):

    def __init__(self, old_model_class, new_model_class):
        self.old_model_class = old_model_class
        self.new_model_class = new_model_class
        self.migrators = {}
        self.check_existing = False
        set_logger(logging.getLogger(self.__class__.__name__))
        self.logger = logging.getLogger(self.__class__.__name__)
        pass

    def print_information(self, message):
        print("Old model class: {}. New model class: {}. {}".format(self.old_model_class.__name__, self.new_model_class.__name__, message))
        self.logger.debug(message)


    def _migrate_model(self, old_model):
        dict = model_to_dict(old_model)
        self.print_information(dict)
        new_existing_model = None
        if self.check_existing:
            new_existing_model = self.get_new_model_by_old_model_id(old_model.id)

        if new_existing_model is None:
            new_model = self.create_model(dict)
            self.print_information(new_model)
            self._save_model(new_model)
            self.create_migrate_information(old_model, new_model)

    def create_migrate_information(self, old_model, new_model):
        migrate_info = common.models.MigrateInformation()
        self.print_information("{}".format(model_to_dict(new_model)))
        self.print_information(new_model.pk)
        migrate_info.old_model_class = self.old_model_class
        migrate_info.new_model_class = self.new_model_class
        migrate_info.old_model_id = old_model.id
        migrate_info.new_model_id = new_model.id
        migrate_info.server_name = Server.name
        migrate_info.save()

    def create_model(self, dict):
        if 'id' in dict:
            del dict['id']
        return self.new_model_class(**dict)

    def migrate_data(self):
        for old_model in self.old_model_class.objects.all().order_by('id'):
            self._migrate_model(old_model)

    def get_old_model_by_id(self, id):
        if id == None:
            return self.old_model_class.objects.first()
        return self.old_model_class.objects.get(id=id)

    def get_new_model_by_id(self, id):
        return self.new_model_class.objects.get(id=id)

    def get_new_model_by_old_model_id(self, id):
        try:
            migrate_information = common.models.MigrateInformation.objects.get( old_model_class=str(self.old_model_class), old_model_id=id, server_name=Server.name)
        except common.models.MigrateInformation.DoesNotExist:
            self.print_information("Cannot find migrate information with id:{}".format(id))
            return None

        new_model = None
        try:
            new_model = self.new_model_class.objects.get(id=migrate_information.new_model_id)
        except self.new_model_class.DoesNotExist:
            self.print_information("Cannot find new_model with id:{}".format(migrate_information.new_model_id))

        return new_model

    def filter_models(self, old_model_dict):
        return self.new_model_class.objects.filter(**old_model_dict)

    def _save_model(self, model):
        model.save()

    def print_count(self):
        self.print_information("Old models: {}. New models: {}".format(self.old_model_class.objects.count(), self.new_model_class.objects.count()))


class DbForeignMigrator(DbMigrator):
    def __init__(self, old_model_class, new_model_class):
        super(DbForeignMigrator, self).__init__(old_model_class, new_model_class)

    def add_migrator(self, field_name, db_migrator):
        self.migrators[field_name] = db_migrator
        return self

    def _migrate_model(self, old_model):
        new_existing_model = None
        if self.check_existing:
            new_existing_model = self.get_new_model_by_old_model_id(old_model.id)
        if new_existing_model is not None:
            return

        dict = model_to_dict(old_model)
        self.print_information("Old model dict: {}".format(dict))
        for field_name, migrator in self.migrators.iteritems():
            if dict[field_name] is None:
                dict[field_name + '_id'] = None
            else:
                new_foreign_model = migrator.get_new_model_by_old_model_id(dict[field_name])
                if new_foreign_model is None:
                    self.print_information("CANNOT FIND MODEL with id: {}, {}".format(field_name, dict[field_name]))
                    return

                dict[field_name + '_id'] = new_foreign_model.id
            del dict[field_name]
        self.print_information("Old model dict with foreign keys: {}".format(dict))

        new_model = self.create_model(dict)
        self._save_model(new_model)
        self.create_migrate_information(old_model, new_model)


class BillMigrator(DbForeignMigrator):
    def __init__(self):
        super(BillMigrator, self).__init__(Bills, common.models.Bill)
        self.add_migrator('kassa', DbMigrator(Kassa, common.models.Kassa))
        pass

    def _save_model(self, model):
        model.admin = common.models.Admin.objects.first()
        model.save()


class TpMigrator(DbForeignMigrator):
    def __init__(self):
        super(TpMigrator, self).__init__(Tp, common.models.Tariff)
        self.add_migrator('bill', BillMigrator())
        self.create_model = self.__create_model
        pass

    def __create_model(self, dict):
        self.print_information("{}".format(dict))
        if 'name' in dict:
            dict['inner_name'] = dict['name']
            del dict['name']
        return super(TpMigrator, self).create_model(dict)

    def filter_models(self, old_model_dict):
        old_model_dict['inner_name'] = old_model_dict['name']
        del old_model_dict['name']
        return super(TpMigrator, self).filter_models(old_model_dict)


class UserMigrator(DbForeignMigrator):
    def __init__(self):
        super(UserMigrator, self).__init__(Users, common.models.User)
        self.add_migrator('bill', BillMigrator())
        self.add_migrator('tp', TpMigrator())
        self.add_migrator('group', DbForeignMigrator(Groups, common.models.Group).add_migrator('bill', BillMigrator()))
        self.create_model = self.__create_model
        pass

    def __create_model(self, dict):
        if 'prefix' in dict:
            del dict['prefix']
        return super(UserMigrator, self).create_model(dict)


class StatementMigrator(DbForeignMigrator):
    def __init__(self):
        super(StatementMigrator, self).__init__(Statements, kassa.models.Statement)
        self.add_migrator('user', UserMigrator())
        pass


class AbonentUserMigrator(DbForeignMigrator):
    def __init__(self):
        super(AbonentUserMigrator, self).__init__(IkAbonentuser, common.models.AbonentUser)
        self.add_migrator('phone', UserMigrator())
        pass


class DetailMigrator(DbForeignMigrator):
    def __init__(self):
        super(DetailMigrator, self).__init__(Details, kassa.models.Detail)
        self.add_migrator('statement', StatementMigrator())
        pass

    def _migrate_model(self, old_model):
        if old_model.dfactor == '':
            old_model.dfactor = None
        else:
            try:
                old_model.dfactor = int(old_model.dfactor)
            except ValueError:
                old_model.dfactor = None
        super(DetailMigrator, self)._migrate_model(old_model)


class SummaryBillStatisticMigrator(DbForeignMigrator):
    def __init__(self, bill_migrator=BillMigrator()):
        super(SummaryBillStatisticMigrator, self).__init__(KassaSummarybillstatistic, kassa.models.SummaryBillStatistic)
        self.add_migrator('bill',bill_migrator)
        self.create_model = self.__create_model

    def __create_model(self, dict):
        if dict.get('users_count', None) is None:
            dict['users_count'] = 0
        return super(SummaryBillStatisticMigrator, self).create_model(dict)


class AccountMigrator(DbForeignMigrator):
    # Когда добавляем сущность, смотрим по AccountType
    # Если User - идем и смотрим ИДы юзера
    # Если Bill - то в Bill
    # Если kassa - то в kassa
    def _migrate_model(self, old_model):
        new_existing_model = None
        if self.check_existing:
            new_existing_model = self.get_new_model_by_old_model_id(old_model.id)
        if new_existing_model is not None:
            return

        dict = model_to_dict(old_model)
        self.print_information("Old model dict: {}".format(dict))
        existing_account_model = self.migrators[dict['type']].get_new_model_by_old_model_id(dict['obj_id'])
        if existing_account_model is None:
            return
            if dict['type'] in [billing.models.Account.BILL, billing.models.Account.BILL_LOST]:
                dict['obj_id'] = common.models.Bill.objects.first().id
            if dict['type'] in [billing.models.Account.KASSA]:
                dict['obj_id'] = common.models.Kassa.objects.first().id
            if dict['type'] in [billing.models.Account.USER, billing.models.Account.USER_PROFIT,billing.models.Account.USER_COST]:
                dict['obj_id'] = common.models.User.objects.first().id

        self.print_information("Old model dict with foreign keys: {}".format(dict))

        dict['obj_id'] = existing_account_model.id
        new_model = self.create_model(dict)
        self._save_model(new_model)
        self.create_migrate_information(old_model, new_model)


class TransactionMigrator(DbForeignMigrator):
    def __init__(self, accounts_migrator, operation_migrator):
        super(TransactionMigrator, self).__init__(Transactions, billing.models.Transaction)
        self.add_migrator('afrom', accounts_migrator)
        self.add_migrator('ato', accounts_migrator)
        self.add_migrator('operation', operation_migrator)



def create_migrators():
    admin_migrator = DbForeignMigrator(Admins, common.models.Admin).add_migrator('kassa',
                                                                                 DbMigrator(Kassa, common.models.Kassa))
    bill_migrator = BillMigrator()
    user_migrator = UserMigrator()
    abonent_user_migrator = AbonentUserMigrator()
    operation_migrator = DbForeignMigrator(Operations, common.models.Operation).add_migrator('owner',
                                                                                             admin_migrator).add_migrator(
        'rowner', admin_migrator)
    tp_migrator = TpMigrator()
    accounts_migrator = AccountMigrator(Accounts, Account)\
        .add_migrator(1, user_migrator)\
        .add_migrator(4, user_migrator)\
        .add_migrator(5, user_migrator)\
        .add_migrator(2, bill_migrator)\
        .add_migrator(6, bill_migrator)\
        .add_migrator(3, DbMigrator(Kassa, common.models.Kassa))

    dfactor_migrator = DbForeignMigrator(Dfactors, kassa.models.Dfactor).add_migrator('bill', bill_migrator)
    return {
        'accounts': accounts_migrator,
        'kassa': DbMigrator(Kassa, common.models.Kassa),
        'admin': admin_migrator,
        'beeline_charges_for_period_order': DbMigrator(BeelineChargesforperiodorder,
                                                       bparser.beeline.models.ChargesForPeriodOrder),
        'beeline_charges_order': DbMigrator(BeelineChargesorder, bparser.beeline.models.ChargesOrder),
        'bill': bill_migrator,
        'groups': DbForeignMigrator(Groups, common.models.Group).add_migrator('bill', bill_migrator),
        'tariff': tp_migrator,
        'user': user_migrator,
        'block': DbForeignMigrator(Block, kassa.models.Block).add_migrator('user', user_migrator),
        'parser_data': DbMigrator(BparserParserdata, bparser.models.ParserData),
        'charge_history': DbForeignMigrator(ChargeHistory, kassa.models.ChargeHistory).add_migrator('user',
                                                                                                    user_migrator),
        'delayed_block': DbForeignMigrator(DelayedBlocks, common.models.DelayedBlock).add_migrator('user',
                                                                                                   user_migrator),
        'statement': StatementMigrator(),
        'dfactor': dfactor_migrator,
        'detail': DetailMigrator(),
        'abonent_user': abonent_user_migrator,
        'abonent_notification': DbForeignMigrator(IkAbonentnotification,
                                                  common.models.AbonentNotification).add_migrator('abonent',
                                                                                                  abonent_user_migrator),
        'oauth_token': DbForeignMigrator(IkOauthtoken, ik.models.OAuthToken).add_migrator('abonent',
                                                                                          abonent_user_migrator),
        'issa_calendar': DbMigrator(IssaCalendar, issa.models.Calendar),
        'issa_parser_session': DbMigrator(IssaParsersession, issa.models.ParserSession),
        'issa_search': DbForeignMigrator(IssaSearch, issa.models.Search).add_migrator('admin',
                                                                                      admin_migrator).add_migrator(
            'bill', bill_migrator),
        'account_mts': DbForeignMigrator(KassaAccountvirtualmanagermts,
                                         kassa.models.AccountVirtualManagerMTS).add_migrator('bill', bill_migrator),
        'mailer_settings': DbForeignMigrator(KassaMailersettings, common.models.MailerSettings).add_migrator('bill',
                                                                                                             bill_migrator),
        'summary_bill_statistic': SummaryBillStatisticMigrator(),
        'log': DbForeignMigrator(Log, common.models.Log).add_migrator('bill', bill_migrator),
        'mailer_box': DbMigrator(MailerBox, mailer.models.Box),
        'message_settings': DbMigrator(MailerMessagesettings, mailer.models.MessageSettings),
        'mail': DbMigrator(Mails, kassa.models.Mail),
        'operation': operation_migrator,
        'payment_history': DbMigrator(PaymentHistory, common.models.PaymentHistory),
        'payments': DbMigrator(Payments, kassa.models.Payment),
        'queue': DbForeignMigrator(Queue, common.models.Queue).add_migrator('user', user_migrator),
        'services': DbForeignMigrator(Services, kassa.models.Service).add_migrator('tp', tp_migrator).add_migrator('dfactor', dfactor_migrator),
        'schedule': DbForeignMigrator(Shedule, kassa.models.Shedule).add_migrator('user', user_migrator),
        'transaction': TransactionMigrator(accounts_migrator, operation_migrator),
        'turn': DbForeignMigrator(Turns, kassa.models.Turn).add_migrator('bill', bill_migrator),
        'update': DbForeignMigrator(Updates, kassa.models.Update).add_migrator('user', UserMigrator())
    }

def migrate_all_models(server_name):
    Server.name = server_name
    migrators = create_migrators()
    models_order = [
        'kassa',
        'admin',
        'beeline_charges_for_period_order',
        'beeline_charges_order',
        'bill',
        'groups',
        'tariff',
        'user',
        'block',
        'parser_data',
        'charge_history',
        'delayed_block',
        'statement',
        'dfactor',
        'detail',
        'abonent_user',
        'abonent_notification',
        'oauth_token',
        'issa_calendar',
        'issa_parser_session',
        'issa_search',
        'account_mts',
        'mailer_settings',
        'summary_bill_statistic',
        'log',
        'mailer_box',
        'message_settings',
        'mail',
        'operation',
        'payment_history',
        'payments',
        'queue',
        'services',
        'schedule',
        'accounts',
        'transaction',
        'turn',
        'update',
    ]

    models = [
        # 'mailer_box', 'update', 'schedule',
        # 'summary_bill_statistic', 'mail', 'services',
        'operation', #'payment_history', 'queue',
        'transaction', 'log', 'payments',
    ]

    for name in models_order:
        migrators[name].check_existing = True
        migrators[name].migrate_data()

    common.models.Bill.objects.all().update(do_mail=False, do_block=False, do_unblock=False, do_update=False)



def get_count_of_all_models():
    migrators = create_migrators()
    for name, migrator in migrators.iteritems():
        migrator.print_count()

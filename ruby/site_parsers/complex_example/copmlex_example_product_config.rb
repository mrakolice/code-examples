#encoding:utf-8
require_relative '../framework/site/product_config'
require_relative 'utilities'


module ComplexExample
  class ComplexExampleProductConfig < Framework::Site::ProductConfig
    def get_page(link)
      begin
        sleep(0.4 + rand(0.8))
        @agent.get(link)
      rescue Mechanize::ResponseCodeError => exception
        if exception.response_code == '404'
          return nil
        else
          raise # Some other error, re-raise
        end
      end
    end

    def get_new_category_type(category, name)
      category_lower = category.mb_chars.downcase.to_s
      name_lower = name.mb_chars.downcase.to_s

      return ['Детям', 'Для девочек', 'РАСПРОДАЖА'] if category_lower.include? 'распродажа' and category_lower.include? 'девочкам'

      if not name_lower.include? 'комплект' or not category_lower.include? 'комплект'
        result = super(get_category_type(category, name))

        if result.length == 2
          result = ['Детям', 'Для мальчиков', 'Одежда'] if category_lower.include? 'мальчикам'
          result = ['Детям', 'Для девочек', 'Одежда'] if category_lower.include? 'девочкам'
        end

        if result[0].include? 'Спорт'
          result[1] = 'Для мальчиков' if category_lower.include? 'мальчикам'
          result[1] = 'Для девочек' if category_lower.include? 'девочкам'
        end

        return result
      end

      return ['Детям', 'Для новорожденных', 'Комплекты одежды'] if category_lower.include? 'малышам'
      return ['Детям', 'Для мальчиков', 'Комплекты одежды'] if category_lower.include? 'мальчикам'

      ['Детям', 'Для девочек', 'Комплекты одежды'] if category_lower.include? 'девочкам'
    end

    def parse_product(product_page, link=nil)
      return nil if product_page.nil?

      result = super(product_page, link)
      return nil if result.category.mb_chars.downcase.to_s.include? 'комплект'
      return nil if result.name.mb_chars.downcase.to_s.include? 'комплект'

      result
    end

    def create_new_product(product_page, link=nil)
      @purchase.check_stop
      product = parse_product(product_page, link)

      return if product.nil?

      colors = ComplexExample::Utilities.get_available_colors(product_page)

      return if colors.nil?

      json = {}

      colors.each do |color_name, color_object|
        next if color_object['sizes'].length == 0

        clone_product = product.clone
        clone_product.name = "#{clone_product.name} Цвет: #{color_name}"
        clone_product.description = "Цвет: #{color_name}. #{clone_product.description}"
        clone_product.sizes = color_object['sizes'].find_all{|s| not s['disabled']}.map {|s| s['text']}

        add_product clone_product

        get_json(clone_product, color_name).each { |key, value| json[key] = value }
      end
      cat_type = get_new_category_type(product.category, product.name)
      add_product_new product, json, cat_type
    end

  end
end
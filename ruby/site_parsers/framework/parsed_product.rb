#encoding:utf-8
# Класс-описание товара (для простоты дальнейшей обработки)
require_relative 'extensions'

class Framework::ParsedProduct
  attr_accessor :category
  attr_accessor :articul
  attr_accessor :images
  attr_accessor :name
  attr_accessor :description
  attr_accessor :price
  attr_accessor :is_added
  attr_accessor :link
  attr_accessor :retail_coefficient
  attr_accessor :markup_coefficient
  attr_accessor :producer
  attr_accessor :composition
  attr_accessor :sizes_link
  attr_accessor :additional

  def initialize(category, articul, images, name, description, price, link=nil, additional={})
    # Категория товара. Берется из сайта
    @category = category || ''
    @link = link
    @is_added = false

    #Последняя часть урла
    @articul = articul || ''

    # Картинки добавляем из сайта
    @images = images.map(&:clone) || Array.new

    # Наименование товара. Уникальное, является ID
    @name = name || ''

    # Первая часть берется из Excel-файла
    # Вторая часть берется из описания на странице
    @description = description || ''

    # Из Excel
    @price = price || 0
    @additional = additional

    @retail_coefficient = 1.7
    @markup_coefficient = 1.3
  end

  def get_json_sizes
    {''=>{'price'=>get_price(@price, @markup_coefficient), 'stock_price' => @price, 'retail_price' => get_price(@price, @retail_coefficient, true),'weight'=>0.2}}
  end
end
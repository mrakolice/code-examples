#encoding:utf-8
require 'rubyXL'
require_relative '../extensions'

class  Framework::Excel::WorksheetConfig
  def initialize

  end

  def with_worksheets(worksheets_func)
    @worksheets_func = worksheets_func
    self
  end

  def run(book)
    @worksheets_func.call(book).ensure_array.map do |worksheet|
      process_worksheet(worksheet)
    end
  end

  def process_worksheet(worksheet)
    []
  end
end
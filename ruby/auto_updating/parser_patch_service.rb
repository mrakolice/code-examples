#encoding:utf-8
module Parser
  class ParserPatchService
    def initialize(provider)
      @provider = provider
      @categories_cache = {}

      @functions = {
          :folder => {
              '-' => ->(d){_delete_folder(d)},
              '+' => ->(d){_add_folder(d)},
          },
          :product=> {
              '-' => ->(d){_delete_product(d)},
              '+' => ->(d){_add_product(d)},
              '~' => ->(d){_modify_product(d)}
          },
          :color=> {
              '-' => ->(d){_delete_color(d)},
              '+' => ->(d){_add_color(d)},
          },
          :sizes=> {
              '-' => ->(d){_delete_size(d)},
              '+' => ->(d){_add_size(d)},
              '~' => ->(d){_modify_size(d)}
          },
          :pics=> {
              '-' => ->(d){_delete_picture(d)},
              '+' => ->(d){_add_picture(d)}
          },
          :color_image=> {
              '-' => ->(d){_delete_picture(d)},
              '+' => ->(d){_add_picture(d, true)}
          }
      }
    end

    def merge(differences)
      @promotion = @provider.promotions.last
      @promotion = _create_promotion(@provider.title) if @promotion.nil?
      # Нужно пробежать по массиву, где первым элементом - тип изменений,
      # вторым - изменение, если - или +, а если ~, то третье - изменение
      # Нужно просто распарсить путь, чтобы стало понятно - какая папка, что изменилось-добавилось и тд
      differences.each do |sign, path, obj1, obj2|
        # дальше в зависимости от количества параметров?
        # folder_name
        # products\cat_type
        # product_sku
        # product_field\color
        # color_name
        # color_fields: sizes\pics\color_image
        # sizes\pics name
        # sizes\pics field or all
        # и для каждого - своя обработка?...мда уж, адская работенка...
        result_object = obj1
        result_object = obj2 if sign == '~'

        raise 'stop merging' if @provider.parser_data.check_stop
        diff_data = Parser::DiffData.new(path, sign, result_object)
        _info diff_data.get_symbol, diff_data.sign, diff_data.result_object
        _info path

        _info 'modify', obj1, obj2 if sign == '~'
        @functions[diff_data.get_symbol][diff_data.sign].call diff_data
        diff_data = nil
      end
      @promotion.update_attribute(:date_end, Time.now + 1.week)
      @provider.parser_data.reload
      @provider.parser_data.update_attribute(:status, 'success')
    rescue => e
      _logger.error e.message
      _logger.info e.backtrace
      raise e
    end

    private

    def _info(*args)
      args.each do |a|
        _logger.info a
      end
    end

    def _logger
      @logger ||= Logger.new("log/ParserPatchWorker_#{@provider.title}.log")
    end

    def _add_folder(diff_data)
      raise 'stop merging' if @provider.parser_data.check_stop
      _info diff_data.get_symbol, diff_data.sign, diff_data.folder_title
      folder_id = Folder.create(title: diff_data.folder_title, promotion: @promotion).id

      diff_data.result_object['products'].each do |product_sku, product_object|
        path = diff_data.get_path('products', product_sku)
        product_object['folder_id'] = folder_id
        product_object['folder_title'] = diff_data.folder_title
        product_diff_data = Parser::DiffData.new(path, '+', product_object)
        _add_product(product_diff_data)
      end
    end

    def _add_product(diff_data)
      raise 'stop merging' if @provider.parser_data.check_stop
      _info diff_data.get_symbol, diff_data.sign, diff_data.result_object
      folder_id = diff_data.result_object['folder_id']
      folder_title = diff_data.result_object['folder_title']

      if folder_id.nil?
        folder = _find_folder(diff_data)
        folder_id = folder.id
        folder_title = folder.title
      end

      categories = diff_data.result_object['cat_type']
      categories << folder_title if diff_data.result_object['cat_type'].length == 2
      category_id = _find_or_create_categories(categories)

      p = Product.find_or_create_by(
          category_id: category_id,
          sku: diff_data.result_object['sku'],
          type_commodity: 'complex',
          folder_id: folder_id
      )
      diff_data.result_object['description'] = 'Без описания' if diff_data.result_object['description'].blank?
      p.update_attributes(
          title: diff_data.result_object['title'],
          description: diff_data.result_object['description'],
          additional: diff_data.result_object['additional'],
          sizes_table: diff_data.result_object['sizes_table'],
          sizes_link: diff_data.result_object['sizes_link'],
          producer: diff_data.result_object['producer'],
          composition: diff_data.result_object['composition']
      )
      _info p.id

      diff_data.result_object['color'].each do |color_name, color_object|
        path = diff_data.get_path('color', color_name)
        color_object['product_id'] = p.id
        color_diff_data = Parser::DiffData.new(path, '+', color_object)
        _add_color(color_diff_data)
      end

    end

    def _add_color(diff_data)
      raise 'stop merging' if @provider.parser_data.check_stop
      _info diff_data.get_symbol, diff_data.sign, diff_data.result_object
      product_id = diff_data.result_object['product_id']
      product_id = _find_product(diff_data).id if product_id.nil?
      _info diff_data.color_name
      color = Color.find_or_create_by(
          product_id: product_id,
          title: diff_data.color_name
      )
      color_id = color.id
      if diff_data.result_object['sizes']
        diff_data.result_object['sizes'].each do |size_name, size_object|
          path = diff_data.get_path('sizes', size_name)
          size_object['color_id'] = color_id
          size_diff_data = Parser::DiffData.new(path, '+', size_object)
          _add_size(size_diff_data)
        end
      end
      diff_data.result_object['pics'].each do |pic_hash, pic_object|
        path = diff_data.get_path('pics', pic_hash)
        pic_object['full_color'] = color
        pic_diff_data = Parser::DiffData.new(path, '+', pic_object)
        _add_picture(pic_diff_data)
      end
      if diff_data.result_object['color_image'].present?
        path = diff_data.get_path('color_image')
        diff_data.result_object['color_image']['full_color'] = color
        pic_diff_data = Parser::DiffData.new(path, '+', diff_data.result_object['color_image'])
        _add_picture(pic_diff_data, true)
      end
    end

    def _add_size(diff_data)
      raise 'stop merging' if @provider.parser_data.check_stop
      _info diff_data.get_symbol, diff_data.sign, diff_data.inner_field, diff_data.result_object
      color_id = diff_data.result_object['color_id']
      color_id = _find_color(diff_data).id if color_id.nil?

      s=Size.find_or_create_by(
          title: diff_data.inner_field,
          color_id: color_id
      )
      s.update_attributes(
          weight: diff_data.result_object['weight'],
          price: diff_data.result_object['price'],
          retail_price: diff_data.result_object['retail_price'],
          stock_price: diff_data.result_object['stock_price']
      )
    end

    def _add_picture(diff_data, is_color_image=false)
      raise 'stop merging' if @provider.parser_data.check_stop
      _info diff_data.get_symbol, diff_data.sign, diff_data.result_object
      color = diff_data.result_object['full_color']
      color = _find_color(diff_data) if color.nil?

      image = color.images.build(img_hash: diff_data.result_object['hash'], color_image: is_color_image)
      image.picture_from_url(diff_data.result_object['path'])
      image.save
    end

    def _modify_product(diff_data)
      raise 'stop merging' if @provider.parser_data.check_stop
      _info diff_data.get_symbol, diff_data.sign, diff_data.result_object
      product = _find_product(diff_data)

      if diff_data.product_field == 'cat_type'
        _change_categories(product, diff_data)
        return
      end
      product.update_attribute(diff_data.product_field, diff_data.result_object)
    end

    def _change_categories(product, diff_data)
      _info diff_data.get_symbol, diff_data.sign, diff_data.result_object
      categories = diff_data.result_object
      categories << _find_folder(diff_data).title if categories.length == 2
      category_id = _find_or_create_categories(categories)
      product.update_attribute(:category_id, category_id)
    end

    def _modify_size(diff_data)
      raise 'stop merging' if @provider.parser_data.check_stop
      _info diff_data.get_symbol, diff_data.sign, diff_data.result_object
      size = _find_size(diff_data)
      size.update_attribute(diff_data.deep_inner_field, diff_data.result_object)
    end

    def _delete_folder(diff_data)
      raise 'stop merging' if @provider.parser_data.check_stop
      _info diff_data.get_symbol, diff_data.sign, diff_data.result_object
      Folder.destroy_all(_folder_hash(diff_data))
    end

    def _delete_product(diff_data)
      raise 'stop merging' if @provider.parser_data.check_stop
      _info diff_data.get_symbol, diff_data.sign, diff_data.result_object
      Product.includes(:folder).destroy_all(_product_hash(diff_data))
    end

    def _delete_color(diff_data)
      return if @provider.parser_data.check_stop
      _info diff_data.get_symbol, diff_data.sign, diff_data.result_object
      Color.includes(product: :folder).destroy_all(_color_hash(diff_data))
    end

    def _delete_size(diff_data)
      raise 'stop merging' if @provider.parser_data.check_stop
      _info diff_data.get_symbol, diff_data.sign, diff_data.result_object
      Size.includes(color: {product: :folder}).destroy_all(_size_hash(diff_data))
    end

    def _delete_picture(diff_data)
      raise 'stop merging' if @provider.parser_data.check_stop
      _info diff_data.get_symbol, diff_data.sign, diff_data.result_object
      _find_picture(diff_data).destroy
    end

    def _find_picture(diff_data)
      _info 'find picture'
      _info diff_data.result_object['hash']
      color = _find_color(diff_data)
      color.images.find_by(img_hash: diff_data.result_object['hash'])
    end

    def _find_size(diff_data)
      _info 'find size'
      _info _size_hash(diff_data)
      Size.includes(color: {product: :folder}).find_by(_size_hash(diff_data))
    end

    def _find_color(diff_data)
      _info 'find color'
      _info _color_hash(diff_data)

      Color.includes(product: :folder).find_by(_color_hash(diff_data))
    end

    def _find_product(diff_data)
      _info 'find product'
      _info _product_hash(diff_data)
      Product.includes(:folder).find_by(_product_hash(diff_data))
    end

    def _find_folder(diff_data)
      _info 'find folder'
      _info  _folder_hash(diff_data)
      Folder.find_by(_folder_hash(diff_data))
    end

    def _find_or_create_categories(cat_type)
      cat_types = cat_type.is_a?(Array) ? cat_type : JSON.parse(cat_type)
      if @categories_cache.include?(cat_types.join('|'))
        @categories_cache[cat_types.join('|')]
      else
        @categories_cache[cat_types.join('|')] = _dig_category(cat_types)
      end
    end

    def _dig_category(categories, parent_id = nil)
      category_id = Category.find_or_create_by(title: categories.first, parent_id: parent_id).id
      categories = categories.drop(1)
      if categories.length > 0
        _dig_category(categories, category_id)
      else
        category_id
      end
    end

    def _folder_hash(diff_data)
      {
          title: diff_data.folder_title.mb_chars.to_s,
          promotion_id: @promotion.id
      }
    end

    def _product_hash(diff_data)
      {
          sku: diff_data.product_sku.mb_chars.to_s,
          folders: _folder_hash(diff_data)
      }
    end

    def _color_hash(diff_data)
      {
          title: diff_data.color_name.mb_chars.to_s,
          products: { sku: diff_data.product_sku.mb_chars.to_s},
          folders: _folder_hash(diff_data)
      }
    end

    def _size_hash(diff_data)
      {
          title: diff_data.inner_field,
          colors: { title: diff_data.color_name.mb_chars.to_s},
          products: { sku: diff_data.product_sku.mb_chars.to_s},
          folders: _folder_hash(diff_data)
      }
    end

    def _image_hash(diff_data)
      {
          img_hash: diff_data.result_object['hash'],
          colors: { title: diff_data.color_name.mb_chars.to_s},
          products: { sku: diff_data.product_sku.mb_chars.to_s},
          folders: _folder_hash(diff_data)
      }
    end

    def _create_promotion(name)
      promotion = @provider.promotions.build(_promotion_params(name))
      promotion.save
      promotion
    end

    def _promotion_params(name)
      {
          title: name,
          status: :new,
          provider_id: @provider.id,
          date_start: Time.now,
          date_end: Time.now + 7.days
      }
    end
  end
end
